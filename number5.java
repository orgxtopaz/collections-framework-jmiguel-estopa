/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package borutoy;

/**
 *
 * @author 2ndyrGroupA
 */
public class number5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        //5.Compare and contrast the classic for loop versus foreach. What are the pros and cons of both sides?
        
        //ANSWER:
        //the difference between for loop and foreach is that for loop can do execute any data and has the privleged  to collection
        //elemets alone. while foreach loop basically iterate or executes each element in a collection in turn.
        
        //PROS AND CONS 
        //---------- FOR lOOP
        //PROS
        // one of the pros is that we will know  exactly how many times the loop will execute before the loop starts.
        //CONS 
        //There are tendency that we can't iterate all of the elements inside the collections since it's execution depends only
        // on the statement.
        //---------- FOREACH
        //PROS
        // one of the pros for the for-each loop is that it exclude the possibility of bugs and makes the code more readable.
        //CONS 
        //  For-each loops do not keep track of an index. Since it always iterate  it cannot obtain an array index. 
        
    }
    
}
