/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package borutoy;

import java.util.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Arrays;
import java.lang.*;

/**
 *
 * @author 2ndyrGroupA
 */
public class number8 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        //8Write a Java function that counts the number of keys in a map that starts with “concordia”.
// E.g. {concordia1: “Value given”, acconcordia: “Test”, condensada: “Sweet”} - returns 1
//
        System.out.println("START OF NUMBER 8");
        // Creating an empty Map
        Map<String, String> map = new HashMap<>();
        
        map.put("concordia", "Sweet");

        map.put("concordia1222", "Estopaz");

        map.put("acconcordia", "Test”");

        map.put("condensada", "Sweet");
        
 

        // 1. Using an iterator
        int count = 0;

        Iterator it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> pairs = (Map.Entry) it.next();
            if (pairs.getKey().substring(0, 9).contains("concordia")) {
                System.out.println(pairs.getKey());

                count++;

            }

        }
        System.out.println("");
        System.out.println("Result");
        System.out.println("There are :" + count + " number of keys with the value of concordia");

    }

}
