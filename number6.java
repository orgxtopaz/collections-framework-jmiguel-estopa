/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package borutoy;
import java.util.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Arrays;

/**
 *
 * @author 2ndyrGroupA
 */
public class number6 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //6.Write a Java function that compares 2 sets. Return a set containing the 2 sets’ common values.
        
        System.out.println("START OF NUMBER 6");
        
        Set<Integer> num1 = new HashSet<Integer>();
        Set<Integer> num2 = new HashSet<Integer>();


        num1.addAll(Arrays.asList(5, 20, 25, 40, 50));
        num2.addAll(Arrays.asList(5, 10, 26, 40, 50));
        System.out.println("Num 1 values: " + num1);
        System.out.println("Num 2 values: " + num2);

        num2.retainAll(num1);
        System.out.println("Common Values: " + num2);
       
   
        System.out.println("");

    }
    
}
